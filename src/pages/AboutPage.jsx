import Nav from "../Nav";

const AboutPage = () => {
  return (
    <div>
      <Nav />
      <h1 className="text-center mt-20 font-bold text-2xl">Halaman About</h1>
    </div>
  );
};

export default AboutPage;
