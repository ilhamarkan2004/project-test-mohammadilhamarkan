import Banner from "../Banner";
import Nav from "../Nav";
import List from "../fragments/List";

const IdeasPage = () => {
  return (
    <div>
      <Nav />
      <div className="mt-16"></div>
      <Banner />
      <div className="mt-16"></div>
      <List />
    </div>
  );
};

export default IdeasPage;
