import Nav from "../Nav";

const WorkPage = () => {
  return (
    <div>
      <Nav />
      <h1 className="text-center mt-20 font-bold text-2xl ">Halaman Work</h1>
    </div>
  );
};

export default WorkPage;
