import Nav from "../Nav";

const ServicesPage = () => {
  return (
    <div>
      <Nav />
      <h1 className="text-center mt-20 font-bold text-2xl">Halaman Services</h1>
    </div>
  );
};

export default ServicesPage;
