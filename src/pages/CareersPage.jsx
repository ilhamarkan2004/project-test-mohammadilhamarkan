import Nav from "../Nav";

const CareersPage = () => {
  return (
    <div>
      <Nav />
      <h1 className="text-center mt-20 font-bold text-2xl">Halaman Careers</h1>
    </div>
  );
};

export default CareersPage;
