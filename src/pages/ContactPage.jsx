import Nav from "../Nav";

const ContactPage = () => {
  return (
    <div>
      <Nav />
      <h1 className="text-center mt-20 font-bold text-2xl">Halaman Contact</h1>
    </div>
  );
};

export default ContactPage;
