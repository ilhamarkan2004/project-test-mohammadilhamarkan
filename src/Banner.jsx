import { useState, useEffect } from "react";

const Banner = () => {
  const [imageLoaded, setImageLoaded] = useState(false);

  useEffect(() => {
    const img = new Image();
    img.src = "images/image.jpg";
    img.onload = () => {
      setImageLoaded(true);
    };
  }, []);

  return (
    <header
      className="flex items-center justify-center h-96 mb-12 bg-fixed bg-left bg-cover relative"
      style={{
        backgroundImage: imageLoaded
          ? 'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("images/image.jpg")'
          : "none",
        clipPath: "polygon(0 0, 100% 0, 100% calc(100% - 10vw), 0 100%)",
      }}
    >
      {imageLoaded ? null : (
        <img
          src="images/image.jpg"
          alt="Background"
          style={{ display: "none" }}
        />
      )}
      <div>
        <h1 className="text-4xl mb-2 text-center tracking-tight leading-none text-white md:text-5xl lg:text-6xl">
          Ideas
        </h1>
        <p className="mb-8 text-lg font-normal text-center text-slate-200 lg:text-xl">
          Where all our great things begin
        </p>
      </div>
    </header>
  );
};

export default Banner;
