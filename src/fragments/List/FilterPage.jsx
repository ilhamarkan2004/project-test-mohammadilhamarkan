import  { useEffect, useState } from "react";

const FilterPage = ({ onItemsPerPageChange }) => {
const [filter, setFilter] = useState("10");

useEffect(() => {
  const storedItemsPerPage = localStorage.getItem("itemsPerPage");
  if (storedItemsPerPage) {
    setFilter(storedItemsPerPage);
  }
}, []);

const handleChangeFilter = (event) => {
  const selectedValue = event.target.value;
  setFilter(selectedValue);
  localStorage.setItem("itemsPerPage", selectedValue);
  onItemsPerPageChange(parseInt(selectedValue));
};

  return (
    <div className="flex items-center space-x-4">
      <label htmlFor="filter" className="text-gray-700 font-semibold text-sm">
        Show per page
      </label>
      <select
        className="border border-black rounded-full px-3 py-1 focus:outline-none focus:ring focus:border-slate-900"
        name="filter"
        value={filter}
        onChange={handleChangeFilter}
      >
        <option value="10">10</option>
        <option value="20">20</option>
        <option value="50">50</option>
      </select>
    </div>
  );
};

export default FilterPage;
