import { useEffect, useState } from "react";
import { IoIosRewind } from "react-icons/io";
import { TbPlayerTrackNextFilled } from "react-icons/tb";
const Pagination = ({ page, onCounterChange }) => {
  const [counter, setCounter] = useState(1);
  const pages = page;

  useEffect(() => {
    setCounter(pages.currentPage);
  }, [pages.currentPage]);

  const previous = () => {
    const newCounter = counter <= 1 ? 1 : counter - 1;
    setCounter(newCounter);
    onCounterChange(newCounter);
  };

  const next = () => {
    const newCounter =
      counter >= pages.totalPage ? pages.totalPage : counter + 1;
    setCounter(newCounter);
    onCounterChange(newCounter);
  };

  const generatePageNumbers = () => {
    const pageNumbers = [];
    for (let i = 1; i <= pages.totalPage; i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  };

  return (
    <nav className="mt-10 flex justify-center">
      <ul className="flex flex-wrap -space-x-px -mx-1">
        <li>
          <button
            className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 "
            onClick={previous}
          >
            <IoIosRewind />
          </button>
        </li>
        {generatePageNumbers().map((pageNumber) => (
          <li key={pageNumber}>
            <button
              className={`flex items-center justify-center px-3 h-8 leading-tight max-w-xs ${
                pageNumber === counter
                  ? "text-white bg-orange-500"
                  : "text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700"
              }`}
              onClick={() => {
                setCounter(pageNumber);
                onCounterChange(pageNumber);
              }}
            >
              {pageNumber}
            </button>
          </li>
        ))}
        <li>
          <button
            className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 "
            onClick={next}
          >
            <TbPlayerTrackNextFilled />
          </button>
        </li>
      </ul>
    </nav>
  );
};
export default Pagination;
