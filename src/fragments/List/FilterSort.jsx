import { useEffect, useState } from "react";

const FilterSort = ({ onSortChange }) => {
  const [sortOption, setSortOption] = useState(() => {
    return localStorage.getItem("sortData") || "Newest";
  });

  useEffect(() => {
    handleSortChange(sortOption);
  }, [sortOption]);

  const handleSortChange = (selectedValue) => {
    setSortOption(selectedValue);
    onSortChange(selectedValue);

    localStorage.setItem("sortData", selectedValue);
  };

  const handleChangeSort = (event) => {
    const selectedValue = event.target.value;
    handleSortChange(selectedValue);
  };

  return (
    <div className="flex items-center space-x-4">
      <label htmlFor="sort" className="text-gray-700 font-semibold text-sm">
        Sort by
      </label>
      <select
        className="border border-black rounded-full px-3 py-1 focus:outline-none focus:ring focus:border-slate-900"
        name="sort"
        value={sortOption}
        onChange={handleChangeSort}
      >
        <option value="Newest">Newest</option>
        <option value="Oldest">Oldest</option>
      </select>
    </div>
  );
};

export default FilterSort;
