

const formatDate = (dateString) => {
  const date = new Date(dateString);
  const day = date.getDate();
  const monthNames = [
    "JANUARY",
    "FEBRUARY",
    "MARCH",
    "APRIL",
    "MAY",
    "JUNE",
    "JULY",
    "AUGUST",
    "SEPTEMBER",
    "OCTOBER",
    "NOVEMBER",
    "DECEMBER",
  ];
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();

  return `${day} ${month} ${year}`;
};

const Card = ({ title, published_at, img }) => {
  const formattedDate = formatDate(published_at);

  return (
    <div className="max-w-xs bg-white border border-gray-200 rounded-lg shadow-lg shadow-gray-300">
      <a href="#">
        <img
          className="rounded-t-lg w-96 h-48 object-cover"
          src={img}
          loading="lazy"
          alt={title || "Image Alt Text"}
        />
      </a>
      <div className="p-4">
        <a href="#">
          <h5 className="mb-1 text-sm font-normal uppercase  text-gray-700">
            {formattedDate}
          </h5>
        </a>
        <p className="mb-2 font-bold text-xl text-black">{title}</p>
      </div>
    </div>
  );
};

export default Card;
