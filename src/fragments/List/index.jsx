import { useEffect, useState } from "react";
import Card from "./Card";
import Pagination from "./Pagination";
import { getIdeas } from "../../services/ideas.services";
import FilterPage from "./FilterPage";
import FilterSort from "./FilterSort";

const List = () => {
  const [ideas, setIdeas] = useState([]);
  const [page, setPage] = useState({
    currentPage: localStorage.getItem("page")
      ? parseInt(localStorage.getItem("page"))
      : 1,
    totalPage: null,
    itemsPerPage: localStorage.getItem("itemsPerPage")
      ? parseInt(localStorage.getItem("itemsPerPage"))
      : 10,
    totalData: null,
    sortData: localStorage.getItem("sortData") || "Newest", 
  });

  const handleCounterChange = (newCounter) => {
    setPage({
      currentPage: newCounter,
      totalPage: page.totalPage,
      itemsPerPage: page.itemsPerPage,
      totalData: page.totalData,
      sortData: page.sortData,
    });
  };

  const handleSortChange = (value) => {
    setPage({
      currentPage: 1,
      totalPage: page.totalPage,
      itemsPerPage: page.itemsPerPage,
      totalData: page.totalData,
      sortData: value,
    });
  };

  useEffect(() => {
    getIdeas(
      (data) => {
        setIdeas(data.data);
        setPage({
          currentPage: data.meta.current_page,
          totalPage: data.meta.last_page,
          itemsPerPage: page.itemsPerPage,
          totalData: data.meta.total,
          sortData: page.sortData,
        });
      },
      page.currentPage,
      page.itemsPerPage,
      getSortingParameter(page.sortData)
    );
  }, [page.currentPage, page.itemsPerPage, page.sortData]);

  const getSortingParameter = (sortData) => {
    return sortData === "Newest" ? "-published_at" : "published_at";
  };

  const handleItemsPerPageChange = (value) => {
    setPage({
      currentPage: 1,
      totalPage: page.totalPage,
      itemsPerPage: value,
      totalData: page.totalData,
      sortData: page.sortData,
    });
  };

  useEffect(() => {
    const lastPage = localStorage.getItem("page");
    const lastItemsPerPage = localStorage.getItem("itemsPerPage");
    const lastSortData = localStorage.getItem("sortData");

    if (lastPage) {
      setPage({
        ...page,
        currentPage: parseInt(lastPage),
        itemsPerPage: lastItemsPerPage
          ? parseInt(lastItemsPerPage)
          : page.itemsPerPage,
        sortData: lastSortData || "Newest",
      });
    }

    if (lastItemsPerPage) {
      setPage({
        ...page,
        currentPage: lastPage ? parseInt(lastPage) : page.currentPage,
        itemsPerPage: parseInt(lastItemsPerPage),
        sortData: lastSortData || "Newest",
      });
    }

    if (!lastPage && !lastItemsPerPage) {
      setPage({
        ...page,
        sortData: lastSortData || "Newest",
      });
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("page", page.currentPage.toString());
    localStorage.setItem("itemsPerPage", page.itemsPerPage.toString());
    localStorage.setItem("sortData", page.sortData);
  }, [page]);

  return (
    <>
      <div className="flex flex-col sm:flex-row justify-between items-center mb-6 sm:mb-10 px-4 sm:px-16">
        <span className="text-sm text-gray-700 mb-4 sm:mb-0">
          Showing{" "}
          <span className="font-semibold text-gray-900 ">
            {(page.currentPage - 1) * page.itemsPerPage + 1}
          </span>{" "}
          -{" "}
          <span className="font-semibold text-gray-900 ">
            {Math.min(
              page.currentPage * page.itemsPerPage,
              page.totalData || 0
            )}
          </span>{" "}
          of{" "}
          <span className="font-semibold text-gray-900 ">{page.totalData}</span>
        </span>
        <div className="flex flex-col sm:flex-row items-center space-y-4 sm:space-y-0 sm:space-x-4">
          <FilterPage onItemsPerPageChange={handleItemsPerPageChange} />
          <FilterSort onSortChange={handleSortChange} />
        </div>
      </div>
      <div className="flex flex-wrap justify-center gap-10 md:px-5 lg:px-7 xl:px-10 px-3">
        {ideas.length > 0 &&
          ideas.map((idea) => (
            <Card
              key={idea.id}
              title={idea.title}
              published_at={idea.published_at}
              img={idea.medium_image?.[0]?.url || "images/image.jpg"}
            />
          ))}
      </div>
      <div className="mb-20 px-10">
        {" "}
        <Pagination page={page} onCounterChange={handleCounterChange} />
      </div>
    </>
  );
};

export default List;
